import {createApp} from 'vue'
import App from "@/App.vue";
import 'element-plus/dist/index.css'
import router from "./router";
import pinia from "@/store";
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import Particles from "particles.vue3";
import "@/style/global/index.scss"
import 'element-plus/theme-chalk/dark/css-vars.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.use(pinia)
app.use(router)
app.use(Particles)
pinia.use(piniaPluginPersistedstate)
app.mount('#app')
