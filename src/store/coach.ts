import {defineStore} from "pinia";
import {onMounted, ref} from "vue";
import {getCoachAPI} from "@/api/destribute.ts";

export const useCoachStore = defineStore('coach', () => {
    const coachList = ref()

    // 获取分类列表
    const getCoachList = async () => {
        const res = await getCoachAPI()
        coachList.value = res.data
    }

    onMounted(() => {
        getCoachList()
    })

    return {
        coachList,
        getCoachList
    }
})