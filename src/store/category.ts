import {onMounted, ref} from "vue";
import {getCategoryAPI} from "@/api/creation.ts";
import {defineStore} from "pinia";
export const useCategoryStore = defineStore('category', () => {
    const categoryList = ref()

    // 获取分类列表
    const getCategoryList = async () => {
        const res = await getCategoryAPI()
        categoryList.value = res.data
    }

    onMounted(() => {
        getCategoryList()
    })

    return {
        categoryList
    }
})