import {defineStore} from "pinia";
import {routerList} from "@/router/router.ts";

export const useRouterStore = defineStore('Router', () => {
    const routerListValue = routerList

    return {
        routerListValue
    }
})