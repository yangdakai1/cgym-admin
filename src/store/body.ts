import {getBodyListAPI} from "@/api/resource";
import {defineStore} from "pinia";
import {onMounted, ref} from 'vue'

export const useBodyStore = defineStore('body', () => {
    const bodyList = ref()

    // 获取部位列表
    const getBodyList = async () => {
        const res = await getBodyListAPI()
        bodyList.value = res.data
    }

    // 设置数据
    const setBodyList = async (val: any) => {
        bodyList.value = val
    }

    onMounted(() => {
        getBodyList()
    })

    return {
        bodyList,
        getBodyList,
        setBodyList
    }
})