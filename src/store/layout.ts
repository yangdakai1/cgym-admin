import {defineStore} from "pinia";
import {ref} from "vue";

export const useLayoutStore = defineStore('layout', () => {
    const menuFold = ref(false)

    return {
        menuFold
    }
})