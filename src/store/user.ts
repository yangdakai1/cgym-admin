import {defineStore} from "pinia";
import {ref} from "vue";
import {useRouter} from "vue-router";

export const useUserStore = defineStore('adminUser', () => {
    const adminInfo = ref()
    const adminToken = ref()
    const router = useRouter()

    const setInfo = (val: any) => {
        adminInfo.value = val
    }

    const setToken = (val: any) => {
        adminToken.value = val
    }

    const adminLogout = () => {
        adminInfo.value = undefined
        adminToken.value = undefined
        router.replace('/login')
    }

    return {
        adminInfo,
        adminToken,
        setInfo,
        setToken,
        adminLogout
    }
}, {
    persist: true
})