import request from "@/utils/request.ts";
import type {addPermission} from "@/type/setting.ts";

enum API {
    // 一级权限
    BASEPERMISSION_URL = '/admin/permission',
    // 二级权限
    BASESUBPERMISSION_URL = '/admin/subPermission',
    // 角色
    BASEROLE_URL = '/admin/role'
}

// 获取权限列表
export const getPermissionAPI = () => request({
    method: 'GET',
    url: API.BASEPERMISSION_URL
})

// 添加一级权限
export const addPermissionAPI = (data: addPermission) => request({
    method: 'POST',
    url: API.BASEPERMISSION_URL,
    data
})

// 删除一级权限
export const deletePermissionAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASEPERMISSION_URL + `/${id}`,
})

// 添加二级权限
export const addSubPermissionAPI = (id: string, data: any) => request({
    method: 'POST',
    url: API.BASESUBPERMISSION_URL + `/${id}`,
    data
})

// 删除二级权限
export const deleteSubPermissionAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASESUBPERMISSION_URL + `/${id}`,
})

// 为角色添加权限
export const addRolePermissionAPI = (data: any) => request({
    method: 'POST',
    url: API.BASEPERMISSION_URL + '/role',
    data
})

// 删除角色权限
export const deleteRolePermissionAPI = (id: string, data: any) => request({
    method: 'DELETE',
    url: API.BASEPERMISSION_URL + `/role/${id}`,
    data
})

// 获取角色列表
export const getRoleAPI = (only = '0') => request({
    method: 'GET',
    url: API.BASEROLE_URL + `?only=${only}`
})

// 添加角色
export const addRoleAPI = (data: any) => request({
    method: 'POST',
    url: API.BASEROLE_URL,
    data
})

// 删除角色
export const deleteRoleAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASEROLE_URL + `/${id}`
})