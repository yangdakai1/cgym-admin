import request from "@/utils/request.ts";
import type {AddAdminItem} from "@/type/destribute.ts";

enum API {
    // 设备路由
    BASEDEVICE_URL = '/admin/device',

    // 教练路由
    BASECOACH_URL = '/admin/coach',

    // 管理员路由
    BASEADMIN_URL = '/admin/auth'
}

// 获取设备列表
export const getDeviceListAPI = () => request({
    method: 'GET',
    url: API.BASEDEVICE_URL
})

// 获取设备详情
export const getDeviceDetailIdAPI = (id: string) => request({
    method: 'GET',
    url: API.BASEDEVICE_URL + `/${id}`
})

// 获取设备详情
export const getDeviceDetailSerialAPI = (serialNumber: string) => request({
    method: 'GET',
    url: API.BASEDEVICE_URL + `/of/detail?serialNumber=${serialNumber}`
})

// 添加设备
export const addDeviceAPI = (data: any) => request({
    method: 'POST',
    url: API.BASEDEVICE_URL,
    data
})

// 删除设备
export const deleteDeviceAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASEDEVICE_URL + `/${id}`
})

// 获取教练列表
export const getCoachAPI = () => request({
    method: 'GET',
    url: API.BASECOACH_URL
})

// 添加教练
export const addCoachAPI = (data: any) => request({
    method: 'POST',
    url: API.BASECOACH_URL,
    data
})

// 修改教练
export const putCoachAPI = (id: string, data: any) => request({
    method: 'PUT',
    url: API.BASECOACH_URL + `/${id}`,
    data
})

// 查询教练
export const searchAPI = (name: string) => request({
    method: 'GET',
    url: API.BASECOACH_URL + `/search?name=${name}`
})

// 删除教练
export const deleteCoachAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASECOACH_URL + `/${id}`
})

// 获取管理员列表
export const getAdminAPI = () => request({
    method: 'GET',
    url: API.BASEADMIN_URL + '/list'
})

// 添加管理员
export const addAdminAPI = (data: AddAdminItem) => request({
    method: 'POST',
    url: API.BASEADMIN_URL,
    data
})

// 删除管理员
export const deleteAdminAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASEADMIN_URL + `/${id}`
})