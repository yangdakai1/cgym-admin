import request from "@/utils/request.ts";
import type {addBodyData, addTutorialData} from "@/type/resource.ts";

enum API {
    // 部位路由
    BASEBODY_URL = '/api/body',

    // 教程路由
    BASETUTORIAL_URL = '/api/tutorial',

    // 课程路由
    BASECOURSE_URL = '/api/course'
}

// 获取部位列表

export const getBodyListAPI = () => request({
    method: 'GET',
    url: API.BASEBODY_URL
})

// 添加部位
export const addBodyAPI = (data: addBodyData) => request({
    method: 'POST',
    url: API.BASEBODY_URL,
    data
})

// 删除部位
export const deleteBodyAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASEBODY_URL + `/${id}`
})

// 获取教程列表
export const getTutorialAPI = () => request({
    method: 'GET',
    url: API.BASETUTORIAL_URL
})

// 获取部位教程
export const getTutorialBodyAPI = (id: string) => request({
    method: 'GET',
    url: API.BASETUTORIAL_URL + `/${id}`
})

// 添加教程
export const addTutorialAPI = (data: addTutorialData) => request({
    method: 'POST',
    url: API.BASETUTORIAL_URL,
    data
})

// 删除教程
export const deleteTutorialAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASETUTORIAL_URL + `/${id}`
})

// 获取课程列表
export const getCourseListAPI = (id = '') => request({
    method: 'GET',
    url: API.BASECOURSE_URL + `?id=${id}`
})

// 添加课程
export const addCourseAPI = (data: any) => request({
    method: 'POST',
    url: API.BASECOURSE_URL,
    data
})

// 删除课程
export const deleteCourseAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASECOURSE_URL + `/${id}`
})