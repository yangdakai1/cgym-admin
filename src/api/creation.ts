import request from "@/utils/request.ts";
import {pagerPrams} from "@/type/user";

enum API {
    // 分类路由
    GETCATEGORY_URL = '/api/category',
    // 二级分类路由
    ADDSUBCATEGORY_URL = '/api/subcategory',
    // 文章路由
    BASEARTICLE_URL = '/api/articles',
    // 搜索路由
    BASESEARCH_URL = '/api/article/keyword'
}

// 获取全部分类
export const getCategoryAPI = () => request({
    method: 'GET',
    url: API.GETCATEGORY_URL
})

// 添加一级分类
export const addCategoryAPI = (data: any) => request({
    method: 'POST',
    url: API.GETCATEGORY_URL,
    data
})

// 添加二级分类
export const addSubCategoryAPI = (data: any, id: string) => request({
    method: 'POST',
    url: API.ADDSUBCATEGORY_URL + `/${id}`,
    data
})

// 删除二级分类
export const deleteSubCategoryAPI = (id: string) => request({
    method: 'DELETE',
    url: API.ADDSUBCATEGORY_URL + `/${id}`
})

// 获取文章列表
export const getArticleAPI = (params: pagerPrams) => request({
    method: 'GET',
    url: API.BASEARTICLE_URL + `?limit=${params.limit}&page=${params.page}`
})

// 关键字搜索
export const getArticleOfKeywordAPI = (keyword: string) => request({
    method: 'GET',
    url: API.BASESEARCH_URL + `?keyword=${keyword}`
})

// 添加文章
export const addArticleAPI = (data: any) => request({
    method: 'POST',
    url: API.BASEARTICLE_URL,
    data
})

// 更新文章
export const putArticleAPI = (data: any) => request({
    method: 'PUT',
    url: API.BASEARTICLE_URL,
    data
})

// 删除文章
export const deleteArticleAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASEARTICLE_URL + `/${id}`
})