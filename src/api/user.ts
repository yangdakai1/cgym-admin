import request from "@/utils/request.ts";
import type {pagerPrams, postLoginData, searchParams, addIdentityParams} from "@/type/user";

enum API {
    // 登录
    LOGIN_URL = '/admin/login',
    // 用户列表
    USERLIST_URL = '/admin/user/list',
    // 查询用户
    SEARCHUSER_LIST = '/admin/user/search',
    // 删除用户
    DELETEUSER_URL = '/admin/user/delete',
    // 身份列表
    BASEIDENTITY_URL = '/api/user/identity/',
    // 记录
    BASERECORD_URL = '/api/record'
}

// 登录
export const postLoginAPI = (data: postLoginData) => request({
    method: 'POST',
    url: API.LOGIN_URL,
    data
})

// 获取用户列表
export const getUserListAPI = (data: pagerPrams) => request({
    method: 'GET',
    url: API.USERLIST_URL + `?limit=${data.limit}&page=${data.page}`,
})

// 查询用户
export const searchUserAPI = (data: searchParams) => request({
    method: 'POST',
    url: API.SEARCHUSER_LIST,
    data
})

// 删除用户
export const deleteUserAPI = (data: string) => request({
    method: 'DELETE',
    url: API.DELETEUSER_URL + `/${data}`
})

// 获取身份列表
export const getIdentityListAPI = (data: pagerPrams) => request({
    method: 'GET',
    url: API.BASEIDENTITY_URL + `?limit=${data.limit}&page=${data.page}`
})

// 添加身份
export const addIdentityAPI = (data: addIdentityParams) => request({
    method: 'POST',
    url: API.BASEIDENTITY_URL,
    data
})

// 删除身份
export const deleteIdentityAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASEIDENTITY_URL + `${id}`
})

// 获取记录列表
export const getRecordAPI = () => request({
    method: 'GET',
    url: API.BASERECORD_URL
})

// 删除记录
export const deleteRecordAPI = (id: string) => request({
    method: 'DELETE',
    url: API.BASERECORD_URL + `/${id}`
})