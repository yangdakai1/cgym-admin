export type deviceItem = {
    _id: string
    name: string
    purchaseDate: string
    purchasePrice: number
    serialNumber: string
    status: string
}

export type AddAdminItem = {
    name: string
    password: string
    phone: string
    role: string
    code: string
}