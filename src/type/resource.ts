export type addBodyData = {
    name: string
    nickname: string
    imgUrl: string
}

type bodyId = {
    _id: string
    nickname: string
}

export type tutorialData = {
    __v: string
    _id: string
    bodyId: bodyId
    desc: string[]
    gender: number
    name: string
    videoUrl: string[]
}

export type addTutorialData = {
    name: string
    gender: number
    bodyId: string
    videoUrl: string[]
    desc: string[]
}
