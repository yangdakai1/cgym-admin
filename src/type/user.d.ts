export type postLoginData = {
    phone: string
    password: string
    checked: boolean
}

export type pagerPrams = {
    limit: number
    page: number
}

export type searchParams = {
    phone: string
}

export type addIdentityParams = {
    name: string
}

export type identityItem = {
    _id:string
    name: string
    createdAt: string
    updatedAt: string
}