export type Meta = {
    name?: string
    icon?: string
    title?: string
    visual?: boolean
}

export type ChildrenRouter = {
    path: string
    redirect?: string
    component?: any
    meta?: Meta
}

export type RouteItem = ChildrenRouter & {
    children?: ChildrenRouter[]
}
