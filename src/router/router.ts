export const routerList = [
    // 首页
    {
        path: '/',
        redirect: '/home',
        component: () => import("@/view/Layout/index.vue"),
        children: [
            {
                path: "/home",
                component: () => import("@/view/Home/index.vue"),
                meta: {
                    name: '首页',
                    icon: 'HomeFilled',
                    title: 'CGYM首页',
                    visual: true
                }
            }
        ]
    },
    // 数据总览
    {
        path: '/panel',
        component: () => import("@/view/Panel/index.vue"),
        redirect: '/panel/data',
        children: [
            {
                path: '/panel/data',
                component: () => import("@/view/Panel/index.vue"),
                meta: {
                    icon: 'Histogram',
                    name: '数据',
                    title: 'CGYM数据大屏',
                    visual: true
                }
            }
        ]
    },
    // 用户
    {
        path: '/user',
        component: () => import("@/view/Layout/index.vue"),
        redirect: '/user/list',
        meta: {
            name: '用户',
            icon: 'UserFilled',
            visual: true
        },
        children: [
            {
                path: '/user/list',
                component: () => import("@/view/User/index.vue"),
                meta: {
                    name: '用户管理',
                    icon: 'User',
                    title: 'CGYM用户管理',
                    visual: true
                }
            },
            {
                path: '/user/identity',
                component: () => import("@/view/Identity/index.vue"),
                meta: {
                    name: '身份管理',
                    icon: 'Sunset',
                    title: 'CGYM身份管理',
                    visual: true
                }
            },
            {
                path: '/user/record',
                component: () => import("@/view/Record/index.vue"),
                meta: {
                    name: '记录管理',
                    icon: 'EditPen',
                    title: 'CGYM记录管理',
                    visual: true
                }
            }
        ]
    },
    // 文章
    {
        path: '/creation',
        component: () => import("@/view/Layout/index.vue"),
        redirect: '/creation/article',
        meta: {
            name: '创作',
            icon: 'Notebook',
            visual: true
        },
        children: [
            {
                path: '/creation/article',
                component: () => import("@/view/Creation/Article/index.vue"),
                meta: {
                    name: '文章管理',
                    icon: 'Memo',
                    title: 'CGYM文章管理',
                    visual: true
                }
            },
            {
                path: '/creation/category',
                component: () => import("@/view/Creation/Category/index.vue"),
                meta: {
                    name: '分类管理',
                    icon: 'Notification',
                    title: 'CGYM用分类管理',
                    visual: true
                }
            }
        ]
    },
    // 分配
    {
        path: '/distribute',
        component: () => import("@/view/Layout/index.vue"),
        redirect: '/distribute/coach',
        meta: {
            name: '分配',
            icon: 'Stamp',
            visual: true
        },
        children: [
            {
                path: '/distribute/coach',
                component: () => import("@/view/Distribute/Coach/index.vue"),
                meta: {
                    name: '教练列表',
                    icon: 'BrushFilled',
                    title: 'CGYM教练列表',
                    visual: true
                }
            },
            {
                path: '/distribute/equip',
                component: () => import("@/view/Distribute/Equip/index.vue"),
                meta: {
                    name: '设备列表',
                    icon: 'Briefcase',
                    title: 'CGYM设备分配',
                    visual: true
                }
            },
            {
                path: '/distribute/administrator',
                component: () => import("@/view/Distribute/Administrator/index.vue"),
                meta: {
                    name: '管理员',
                    icon: 'User',
                    title: 'CGYM管理员',
                    visual: true
                }
            }
        ]
    },
    // 资源
    {
        path: '/resource',
        component: () => import("@/view/Layout/index.vue"),
        redirect: '/resource/course',
        meta: {
            name: '资源管理',
            icon: 'Filter',
            visual: true
        },
        children: [
            {
                path: '/resource/course',
                component: () => import("@/view/Resource/Course/index.vue"),
                meta: {
                    name: '课程管理',
                    icon: 'List',
                    title: 'CGYM课程管理',
                    visual: true
                }
            },
            {
                path: '/resource/tutorial',
                component: () => import("@/view/Resource/Tutorial/index.vue"),
                meta: {
                    name: '教程管理',
                    icon: 'Soccer',
                    title: 'CGYM教程管理',
                    visual: true
                }
            },
            {
                path: '/resource/body',
                component: () => import("@/view/Resource/Body/index.vue"),
                meta: {
                    name: '部位管理',
                    icon: 'Aim',
                    title: 'CGYM部位管理',
                    visual: true
                }
            }
        ]
    },
    // 设置
    {
        path: '/setting',
        component: () => import("@/view/Layout/index.vue"),
        redirect: '/setting/account',
        meta: {
            icon: 'Tools',
            visual: true,
            name: '权限'
        },
        children: [
            {
                path: '/setting/auth',
                component: () => import("@/view/Setting/components/Auth.vue"),
                meta: {
                    name: '权限管理',
                    icon: 'Avatar',
                    title: 'CGYM权限设置',
                    visual: true
                }
            },
            {
                path: '/setting/role',
                component: () => import("@/view/Setting/components/Role.vue"),
                meta: {
                    name: '角色权限',
                    icon: 'HelpFilled',
                    title: 'CGYM系统设置',
                    visual: true
                }
            }
        ]
    },
]