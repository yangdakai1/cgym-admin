export const otherRouter = [
    // 登录页
    {
        path: '/login',
        component: () => import("@/view/Login/index.vue"),
        meta: {
            title: '后台登录'
        }
    },
    // 协议
    {
        path: '/agreement',
        component: () => import("@/view/Agreement/index.vue"),
        meta: {
            title: '隐私协议'
        }
    },
    // 404
    {
        path: '/404',
        component: () => import("@/view/NotFound/index.vue"),
        meta: {
            title: '页面未找到'
        }
    },
    // 其他
    {
        path: '/:patMatch(.*)*',
        redirect: '/404',
        meta: {
            title: '页面未找到'
        }
    }
]
