import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import AutoImport from "unplugin-auto-import/vite"
import Components from "unplugin-vue-components/vite"
import {ElementPlusResolver} from "unplugin-vue-components/resolvers";

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        AutoImport({
            resolvers: [ElementPlusResolver()],
        }),
        Components({
            resolvers: [ElementPlusResolver({ importStyle: "sass" })],
        }),
    ],
    resolve: {
        alias: {
            "@": path.resolve("./src") // 相对路径别名配置
        }
    },
    css: {
        preprocessorOptions: {
            scss: {
                javascriptEnable: true,
                // 导入
                additionalData:
                    `@use "@/style/element/index.scss" as *;`
            }
        }
    }
})
