```
+---public 静态资源
\---src 目录
   +---api 接口
   +---components 公共组件
   +---router 路由
   +---store 仓库
   +---style 样式
   |   +---element
   |   \---global
   +---type 类型
   +---utils 工具
   \---view 视图
      +---Creation 
      |   +---Article 文章管理
      |   \---Category 分类管理
      +---Distribute
      |   +---Coach 教练管理
      |   \---Equip 设备管理
      +---Home 首页
      +---Identity 身份管理
      +---Layout 布局
      |   \---components
      +---Login 登录
      |   \---components
      +---NotFound 未找到
      +---Panel 数据面板
      |   \---components
      |       +---center
      |       +---left
      |       \---right
      +---Record 记录管理
      +---Resource
      |   +---Body 部位管理
      |   +---Course 课程管理
      |   \---Tutorial 教程管理
      +---Setting 权限
      |   \---components
      \---User 用户管理
```
**本项目大致包含上述列举的模块，下面用一些图片来展示部分后台功能，
有想用来使用或测试的可以向我私信，本人可提供管理员账号**
### 登录
![img_5.png](pic/img_5.png)

### 数据页
![img.png](pic/img.png)

### 用户管理
![img_1.png](pic/img_1.png)

### 记录管理
![img_2.png](pic/img_2.png)

### 分类管理
![img_3.png](pic/img_3.png)

### 权限管理
![img_4.png](pic/img_4.png)
